/*
 Navicat Premium Data Transfer

 Source Server         : 项管162.116
 Source Server Type    : MySQL
 Source Server Version : 50623
 Source Host           : 180.76.162.116:3306
 Source Schema         : questionnairesurvey

 Target Server Type    : MySQL
 Target Server Version : 50623
 File Encoding         : 65001

 Date: 21/04/2022 09:13:03
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for graphical_user
-- ----------------------------
DROP TABLE IF EXISTS `graphical_user`;
CREATE TABLE `graphical_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户表格主键',
  `user_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户code(登录使用)',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户姓名',
  `password` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户登录密码',
  `description` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户描述',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '用户状态 0-正常 1-禁用 2-删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '后台用户表' ROW_FORMAT = COMPACT;

SET FOREIGN_KEY_CHECKS = 1;
