package com.system.questionnairesurvey.service;

import com.system.questionnairesurvey.common.util.response.R;
import com.system.questionnairesurvey.entity.mybatis.AnswerEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 答案表 服务类
 * </p>
 *
 * @author shiyi
 * @since 2022-09-16
 */
public interface AnswerService extends IService<AnswerEntity> {

    /**
     * 新增
     */
    R saveData(AnswerEntity entity);

    /**
     * 查询
     */
    R listData(String key, Integer num, Integer size);

    /**
     * 更新
     */
    R updateData(AnswerEntity entity);
}
