package com.system.questionnairesurvey.service.impl;

import com.system.questionnairesurvey.entity.mybatis.SysLogEntity;
import com.system.questionnairesurvey.mapper.SysLogMapper;
import com.system.questionnairesurvey.service.SysLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wlh
 * @since 2022-04-19
 */
@Service
public class SysLogServiceImpl extends ServiceImpl<SysLogMapper, SysLogEntity> implements SysLogService {

}
