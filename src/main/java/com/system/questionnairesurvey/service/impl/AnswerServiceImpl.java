package com.system.questionnairesurvey.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.system.questionnairesurvey.common.util.response.R;
import com.system.questionnairesurvey.entity.mybatis.AnswerEntity;
import com.system.questionnairesurvey.mapper.AnswerMapper;
import com.system.questionnairesurvey.service.AnswerService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 答案表 服务实现类
 * </p>
 *
 * @author shiyi
 * @since 2022-09-16
 */
@Service
public class AnswerServiceImpl extends ServiceImpl<AnswerMapper, AnswerEntity> implements AnswerService {

    @Override
    public R saveData(AnswerEntity entity) {
        return R.buildSuccess(this.save(entity));
    }

    @Override
    public R listData(String key, Integer num, Integer size) {
        PageHelper.startPage(num, size);
        List<AnswerEntity> entities = list();
        return R.buildSuccess(new PageInfo<>(entities));
    }


    @Override
    public R updateData(AnswerEntity entity) {
        return R.buildSuccess(this.updateById(entity));
    }
}
