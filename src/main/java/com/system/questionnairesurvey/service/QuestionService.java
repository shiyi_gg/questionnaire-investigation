package com.system.questionnairesurvey.service;

import com.system.questionnairesurvey.common.util.response.R;
import com.system.questionnairesurvey.entity.mybatis.QuestionEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 问题表 服务类
 * </p>
 *
 * @author shiyi
 * @since 2022-09-16
 */
public interface QuestionService extends IService<QuestionEntity> {

    /**
     * 修改
     */
    R updateQuestion(QuestionEntity entity);

    /**
     * 新增
     */
    R saveQuestion(QuestionEntity entity);

    /**
     * 查询
     */
    R listQuestion(String key, Integer num, Integer size);
}
