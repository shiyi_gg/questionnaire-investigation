package com.system.questionnairesurvey.service;

import com.system.questionnairesurvey.common.util.response.R;
import com.system.questionnairesurvey.entity.mybatis.ProjectEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 项目表 服务类
 * </p>
 *
 * @author shiyi
 * @since 2022-09-16
 */
public interface ProjectService extends IService<ProjectEntity> {
    /**
     * 查询项目
     */
    R listProject(String key, Integer num, Integer size);

    /**
     * 新增项目
     */
    R saveProject(ProjectEntity entity);

    /**
     * 修改项目
     */
    R updateProject(ProjectEntity entity);
}
