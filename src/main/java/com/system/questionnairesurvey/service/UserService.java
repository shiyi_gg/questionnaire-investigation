package com.system.questionnairesurvey.service;

import com.system.questionnairesurvey.common.util.response.R;
import com.system.questionnairesurvey.entity.mybatis.UserEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 后台用户表 服务类
 * </p>
 *
 * @author wlh
 * @since 2022-04-20
 */
public interface UserService extends IService<UserEntity> {

    /**
     * 用户登录
     * @param userCode 用户编码
     * @param password 密码
     * @return R
     */
    R login(String userCode, String password);

    /**
     * 新增用户
     * @param user 用户
     * @return R
     */
    R saveUser(UserEntity user);

    /**
     * 修改用户
     * @param user 用户
     * @return R
     */
    R updateUser(UserEntity user);

    /**
     * 修改密码
     * @param id 用户id
     * @param oldPwd 旧密码
     * @param newPwd 新密码
     * @param renewPwd 确认密码
     * @return R
     */
    R changPwd(Integer id, String oldPwd, String newPwd, String renewPwd);


    /**
     * 分页查询用户列表
     */
    R listUsers(String key, Integer num, Integer size);
}
