package com.system.questionnairesurvey.common.exception;

import com.system.questionnairesurvey.common.util.response.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

/**
 * @Date 2022/4/20 11:03
 * @Created by wlh
 */
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(Exception.class)
    public R handleException(Exception e, HttpServletRequest request) {
        log.error(request.getRequestURI() + ":服务运行异常------> {}", e);
        return R.buildFail("服务运行异常" + e.getMessage());
    }


    /**
     * 自定义异常处理
     */
    @ExceptionHandler(BusinessException.class)
    public R handleException(BusinessException e, HttpServletRequest request) {
        log.error(request.getRequestURI() + ":自定义内部异常------> {}", e);
        return R.buildFail(e.getMsg());
    }


}
