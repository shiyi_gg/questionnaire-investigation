package com.system.questionnairesurvey.common.util.validate;

import com.system.questionnairesurvey.common.util.encryption.EncryptUtils;
import com.system.questionnairesurvey.entity.mybatis.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class SessionUtil {

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    /**
     * 获取用户
     */
    public UserEntity getUser(HttpServletRequest request) {
        String token = request.getHeader("token");
        String decodeAccessToken = EncryptUtils.decodeBase64(token);
        return (UserEntity) redisTemplate.opsForValue().get(decodeAccessToken);
    }


    public void removeUser(HttpServletRequest request) {
        String token = request.getHeader("token");
        String decodeAccessToken = EncryptUtils.decodeBase64(token);
        redisTemplate.delete(decodeAccessToken);
    }
}
