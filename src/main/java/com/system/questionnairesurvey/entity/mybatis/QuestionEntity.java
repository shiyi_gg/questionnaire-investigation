package com.system.questionnairesurvey.entity.mybatis;

import com.baomidou.mybatisplus.annotation.*;

import java.util.Date;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * 问题表
 * </p>
 *
 * @author shiyi
 * @since 2022-09-16
 */
@Data
@TableName("question")
@ApiModel(value="QuestionEntity对象", description="问题表")
public class QuestionEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "问题名称")
    @TableField("name")
    private String name;

    @ApiModelProperty(value = "问题类型：1单选，2多选，3解答")
    @TableField("type")
    private Integer type;

    @ApiModelProperty(value = "问题描述")
    @TableField("describe")
    private String describe;

    @ApiModelProperty(value = "分值")
    @TableField("score")
    private Integer score;

    @ApiModelProperty(value = "创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
}
