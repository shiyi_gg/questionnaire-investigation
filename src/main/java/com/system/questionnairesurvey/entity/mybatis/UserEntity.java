package com.system.questionnairesurvey.entity.mybatis;

import com.baomidou.mybatisplus.annotation.*;

import java.util.Date;
import java.io.Serializable;

import com.system.questionnairesurvey.common.util.validate.group.AddGroup;
import com.system.questionnairesurvey.common.util.validate.group.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * <p>
 * 后台用户表
 * </p>
 *
 * @author wlh
 * @since 2022-04-20
 */
@Data
@TableName("user")
@ApiModel(value="GraphicalUserEntity对象", description="后台用户表")
public class UserEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户表格主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @NotBlank(message = "用户名不能为空", groups = {AddGroup.class, UpdateGroup.class})
    @ApiModelProperty(value = "用户code(登录使用)")
    @TableField("user_code")
    private String userCode;

    @NotBlank(message = "用户姓名不能为空", groups = {AddGroup.class, UpdateGroup.class})
    @ApiModelProperty(value = "用户姓名")
    @TableField("name")
    private String name;

    @ApiModelProperty(value = "用户登录密码")
    @TableField("password")
    private String password;

    @ApiModelProperty(value = "用户描述")
    @TableField("description")
    private String description;

    @ApiModelProperty(value = "创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    @ApiModelProperty(value = "用户状态 0-正常 1-禁用 2-删除")
    @TableField("status")
    private Integer status;

    @ApiModelProperty(value = "用户token")
    @TableField(exist = false)
    private String token;


}
