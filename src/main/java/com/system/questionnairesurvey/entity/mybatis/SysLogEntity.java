package com.system.questionnairesurvey.entity.mybatis;

import com.baomidou.mybatisplus.annotation.*;

import java.util.Date;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * 
 * </p>
 *
 * @author wlh
 * @since 2022-04-19
 */
@Data
@TableName("sys_log")
@ApiModel(value="SysLogEntity对象", description="日志对象")
public class SysLogEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "操作人")
    @TableField("user_code")
    private String userCode;

    @ApiModelProperty(value = "操作内容")
    @TableField("operation")
    private String operation;

    @ApiModelProperty(value = "方法")
    @TableField("method")
    private String method;

    @ApiModelProperty(value = "参数")
    @TableField("params")
    private String params;

    @ApiModelProperty(value = "执行时长（毫秒）")
    @TableField("time")
    private Integer time;

    @ApiModelProperty(value = "返回结果")
    @TableField("res")
    private String res;

    @ApiModelProperty(value = "访问的ip地址")
    @TableField("ip")
    private String ip;

    @ApiModelProperty(value = "创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;

    @ApiModelProperty(value = "创建时间")
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

}
