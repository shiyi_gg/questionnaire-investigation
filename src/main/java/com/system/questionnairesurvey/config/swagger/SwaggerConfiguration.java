package com.system.questionnairesurvey.config.swagger;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

/**
 * @Date 2022/3/1 15:40
 * @Created by wlh
 */

@Configuration
//@EnableSwagger2
//@EnableSwaggerBootstrapUI
@EnableSwagger2WebMvc
public class SwaggerConfiguration {

    @Value("${swagger.enable}")
    private Boolean swaggerEnable;

    // admin后台
    @Bean
    public Docket createRestApiAdmin() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("admin")
                .host("/")
                .apiInfo(apiInfo())
                .enable(swaggerEnable)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.system.questionnairesurvey.controller.admin"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("questionnairesurvey")
                .description("questionnairesurvey API DOCUMENT")
                .termsOfServiceUrl("http://host:port/")
                .contact(new Contact("shiyi", null, null))
                .version("1.0.0")
                .build();
    }

}
