package com.system.questionnairesurvey.config.interceptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @Date 2022/4/20 11:16
 * @Created by wlh
 */
@Component
public class GlobalInterceptor implements WebMvcConfigurer {

    @Autowired
    private InterceptorConfig interceptorConfig;

    private String[] interceptorPaths = {
            "/admin/**"
    };
    private String[] excludePathPatterns = {
            "/admin/user/login"
    };

    // 配置拦截器拦截的路径
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(interceptorConfig)
                .addPathPatterns(interceptorPaths)
                .excludePathPatterns(excludePathPatterns);
    }
}
