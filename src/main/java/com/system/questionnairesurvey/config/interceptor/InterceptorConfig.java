package com.system.questionnairesurvey.config.interceptor;

import com.system.questionnairesurvey.common.exception.BusinessException;
import com.system.questionnairesurvey.common.util.validate.SessionUtil;
import com.system.questionnairesurvey.entity.mybatis.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Objects;

/**
 * @Date 2022/4/20 11:18
 * @Created by wlh
 */
@Component
public class InterceptorConfig implements HandlerInterceptor {

    @Autowired
    private SessionUtil sessionUtil;

    // true代表放行, false是拦截
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        UserEntity user = sessionUtil.getUser(request);
        if (Objects.isNull(user)) {
            throw new BusinessException("用户未登录");
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }

}
