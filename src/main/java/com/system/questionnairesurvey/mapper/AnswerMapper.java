package com.system.questionnairesurvey.mapper;

import com.system.questionnairesurvey.entity.mybatis.AnswerEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 答案表 Mapper 接口
 * </p>
 *
 * @author shiyi
 * @since 2022-09-16
 */
public interface AnswerMapper extends BaseMapper<AnswerEntity> {

}
