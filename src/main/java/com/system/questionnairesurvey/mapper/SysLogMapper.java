package com.system.questionnairesurvey.mapper;

import com.system.questionnairesurvey.entity.mybatis.SysLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wlh
 * @since 2022-04-19
 */
public interface SysLogMapper extends BaseMapper<SysLogEntity> {

}
