package com.system.questionnairesurvey.mapper;

import com.system.questionnairesurvey.entity.mybatis.ProjectEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 项目表 Mapper 接口
 * </p>
 *
 * @author shiyi
 * @since 2022-09-16
 */
public interface ProjectMapper extends BaseMapper<ProjectEntity> {

}
