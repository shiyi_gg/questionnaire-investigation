package com.system.questionnairesurvey.mapper;

import com.system.questionnairesurvey.entity.mybatis.UserEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 后台用户表 Mapper 接口
 * </p>
 *
 * @author wlh
 * @since 2022-04-20
 */
public interface UserMapper extends BaseMapper<UserEntity> {

    UserEntity queryUserByUserCode(@Param("userCode") String userCode, @Param("userId") Integer userId);
}
