package com.system.questionnairesurvey.mapper;

import com.system.questionnairesurvey.entity.mybatis.QuestionEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 问题表 Mapper 接口
 * </p>
 *
 * @author shiyi
 * @since 2022-09-16
 */
public interface QuestionMapper extends BaseMapper<QuestionEntity> {

}
