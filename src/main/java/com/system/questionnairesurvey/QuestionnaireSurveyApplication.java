package com.system.questionnairesurvey;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.system.questionnairesurvey.mapper")
public class QuestionnaireSurveyApplication {

    public static void main(String[] args) {
        SpringApplication.run(QuestionnaireSurveyApplication.class, args);
    }

}
