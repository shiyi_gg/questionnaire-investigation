package com.system.questionnairesurvey.controller.admin;


import com.system.questionnairesurvey.common.log.annotation.SysLog;
import com.system.questionnairesurvey.common.util.response.R;
import com.system.questionnairesurvey.common.util.validate.SessionUtil;
import com.system.questionnairesurvey.common.util.validate.ValidatorUtils;
import com.system.questionnairesurvey.common.util.validate.group.AddGroup;
import com.system.questionnairesurvey.common.util.validate.group.UpdateGroup;
import com.system.questionnairesurvey.entity.mybatis.UserEntity;
import com.system.questionnairesurvey.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 后台用户表 前端控制器
 * </p>
 *
 * @author wlh
 * @since 2022-04-20
 */
@Slf4j
@Api(tags = "后台用户管理")
@RestController
@RequestMapping("/admin/user")
@AllArgsConstructor
public class UserController {

    private final UserService userService;
    private final SessionUtil sessionUtil;

    @SysLog("用户登录")
    @PostMapping("/login")
    @ApiOperation(value = "用户登录")
    public R login(
            @ApiParam(value = "用户登录名", required = true) @RequestParam("userCode") String userCode,
            @ApiParam(value = "用户登录密码", required = true) @RequestParam("password") String password) {
        return userService.login(userCode, password);
    }

    @GetMapping("/list")
    @SysLog("查询用户列表")
    @ApiOperation("查询用户列表")
    public R list(@ApiParam("查询关键字") @RequestParam(value = "key",required = false) String key,
                  @ApiParam("当前页(默认第一页)") @RequestParam(value = "num", defaultValue = "1") Integer num,
                  @ApiParam("页面行数(默认10行)") @RequestParam(value = "size", defaultValue = "10") Integer size) {
        return userService.listUsers(key, num, size);
    }

    @SysLog("新增用户")
    @PostMapping("/save")
    @ApiOperation(value = "新增用户")
    public R addUser(@RequestBody @ApiParam("GraphicalUserEntity请求实体") UserEntity user) {
        ValidatorUtils.validateEntity(user, AddGroup.class);
        return userService.saveUser(user);
    }

    @SysLog("修改用户")
    @PostMapping("/update")
    @ApiOperation(value = "修改用户")
    public R updateUser(@RequestBody @ApiParam("GraphicalUserEntity请求实体") UserEntity user) {
        ValidatorUtils.validateEntity(user, UpdateGroup.class);
        return userService.updateUser(user);
    }

    @SysLog("退出登录")
    @PostMapping(value = "/logout")
    @ApiOperation(value = "退出登录")
    public R userLoginOutBack(HttpServletRequest request) {
        sessionUtil.removeUser(request);
        return R.buildSuccess("退出登录成功");
    }

    @SysLog("修改密码")
    @PostMapping(value = "/changePwd")
    @ApiOperation(value = "修改密码")
    public R changePwd(@ApiParam(value = "用户id", required = true) @RequestParam Integer id,
                       @ApiParam(value = "旧密码", required = true) @RequestParam String oldPwd,
                       @ApiParam(value = "新密码", required = true) @RequestParam String newPwd,
                       @ApiParam(value = "确认密码", required = true) @RequestParam String renewPwd) {
        return userService.changPwd(id, oldPwd, newPwd, renewPwd);
    }


}

