package com.system.questionnairesurvey.controller.admin;

import com.system.questionnairesurvey.common.log.annotation.SysLog;
import com.system.questionnairesurvey.common.util.response.R;
import com.system.questionnairesurvey.common.util.validate.ValidatorUtils;
import com.system.questionnairesurvey.common.util.validate.group.AddGroup;
import com.system.questionnairesurvey.common.util.validate.group.UpdateGroup;
import com.system.questionnairesurvey.entity.mybatis.QuestionEntity;
import com.system.questionnairesurvey.service.QuestionService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 问题表 前端控制器
 * </p>
 *
 * @author shiyi
 * @since 2022-09-16
 */
@RestController
@RequestMapping("/question")
public class QuestionController {

    @Autowired
    private QuestionService service;

    @GetMapping
    @ApiOperation("查询问题列表")
    public R list(@ApiParam("查询关键字") @RequestParam(value = "key",required = false) String key,
                  @ApiParam("当前页(默认第一页)") @RequestParam(value = "num", defaultValue = "1") Integer num,
                  @ApiParam("页面行数(默认10行)") @RequestParam(value = "size", defaultValue = "10") Integer size) {
        return service.listQuestion(key, num, size);
    }

    @SysLog("新增问题")
    @PostMapping("/save")
    @ApiOperation(value = "新增问题")
    public R add(@RequestBody QuestionEntity entity) {
        ValidatorUtils.validateEntity(entity, AddGroup.class);
        return service.saveQuestion(entity);
    }

    @SysLog("修改问题")
    @PostMapping("/update")
    @ApiOperation(value = "修改问题")
    public R update(@RequestBody QuestionEntity entity) {
        ValidatorUtils.validateEntity(entity, UpdateGroup.class);
        return service.updateQuestion(entity);
    }

    @SysLog("删除问题")
    @PostMapping("/delete")
    @ApiOperation(value = "删除问题")
    public R delete(@RequestParam Integer questionId) {
        return R.buildSuccess(service.removeById(questionId));
    }
}

