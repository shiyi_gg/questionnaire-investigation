package com.system.questionnairesurvey.controller.admin;

import com.system.questionnairesurvey.common.log.annotation.SysLog;
import com.system.questionnairesurvey.common.util.response.R;
import com.system.questionnairesurvey.common.util.validate.ValidatorUtils;
import com.system.questionnairesurvey.common.util.validate.group.AddGroup;
import com.system.questionnairesurvey.common.util.validate.group.UpdateGroup;
import com.system.questionnairesurvey.entity.mybatis.ProjectEntity;
import com.system.questionnairesurvey.service.ProjectService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 项目表 前端控制器
 * </p>
 *
 * @author shiyi
 * @since 2022-09-16
 */
@Api(tags = "项目")
@RestController
@RequestMapping("/project")
public class ProjectController {

    @Autowired
    private ProjectService service;

    @GetMapping
    @ApiOperation("查询项目列表")
    public R list(@ApiParam("查询关键字") @RequestParam(value = "key",required = false) String key,
                  @ApiParam("当前页(默认第一页)") @RequestParam(value = "num", defaultValue = "1") Integer num,
                  @ApiParam("页面行数(默认10行)") @RequestParam(value = "size", defaultValue = "10") Integer size) {
        return service.listProject(key, num, size);
    }

    @SysLog("新增项目")
    @PostMapping("/save")
    @ApiOperation(value = "新增用户")
    public R addUser(@RequestBody ProjectEntity entity) {
        ValidatorUtils.validateEntity(entity, AddGroup.class);
        return service.saveProject(entity);
    }

    @SysLog("修改项目")
    @PostMapping("/update")
    @ApiOperation(value = "修改项目")
    public R updateUser(@RequestBody ProjectEntity entity) {
        ValidatorUtils.validateEntity(entity, UpdateGroup.class);
        return service.updateProject(entity);
    }

    @SysLog("删除项目")
    @PostMapping("/delete")
    @ApiOperation(value = "删除项目")
    public R delete(@RequestParam Integer projectId) {
        return R.buildSuccess(service.removeById(projectId));
    }
}

