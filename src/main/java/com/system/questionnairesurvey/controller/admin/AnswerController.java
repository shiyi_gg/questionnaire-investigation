package com.system.questionnairesurvey.controller.admin;

import com.system.questionnairesurvey.common.log.annotation.SysLog;
import com.system.questionnairesurvey.common.util.response.R;
import com.system.questionnairesurvey.common.util.validate.ValidatorUtils;
import com.system.questionnairesurvey.common.util.validate.group.AddGroup;
import com.system.questionnairesurvey.common.util.validate.group.UpdateGroup;
import com.system.questionnairesurvey.entity.mybatis.AnswerEntity;
import com.system.questionnairesurvey.service.AnswerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 答案表 前端控制器
 * </p>
 *
 * @author shiyi
 * @since 2022-09-16
 */
@Api(tags = "答案")
@RestController
@RequestMapping("/answer")
public class AnswerController {

    @Autowired
    private AnswerService service;

    @GetMapping
    @ApiOperation("查询列表")
    public R list(@ApiParam("查询关键字") @RequestParam(value = "key",required = false) String key,
                  @ApiParam("当前页(默认第一页)") @RequestParam(value = "num", defaultValue = "1") Integer num,
                  @ApiParam("页面行数(默认10行)") @RequestParam(value = "size", defaultValue = "10") Integer size) {
        return service.listData(key, num, size);
    }

    @SysLog("新增答案")
    @PostMapping("/save")
    @ApiOperation(value = "新增答案")
    public R add(@RequestBody AnswerEntity entity) {
        ValidatorUtils.validateEntity(entity, AddGroup.class);
        return service.saveData(entity);
    }

    @SysLog("修改答案")
    @PostMapping("/update")
    @ApiOperation(value = "修改答案")
    public R update(@RequestBody AnswerEntity entity) {
        ValidatorUtils.validateEntity(entity, UpdateGroup.class);
        return service.updateData(entity);
    }

    @SysLog("删除答案")
    @PostMapping("/delete")
    @ApiOperation(value = "删除答案")
    public R delete(@RequestParam Integer questionId) {
        return R.buildSuccess(service.removeById(questionId));
    }
}

